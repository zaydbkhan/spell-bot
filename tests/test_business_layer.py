# https://stackoverflow.com/a/64296521

import unittest

from app.business_layer import SpellChecker, MessageGenerator
from app.data_layer import MockDatabaseManager, MockWiktionaryManager


class SpellCheckerTest(unittest.TestCase):

    def test_loading_words(self):
        # should return all as misspelled as it doesn't recognize these words by default
        mgr = SpellChecker(MockDatabaseManager([]), MockWiktionaryManager([]))
        self.assertEqual(
            mgr.get_misspelled(["ok", "wtf", "lmao"]), ["ok", "wtf", "lmao"]
        )

        # this should return an empty list
        mgr = SpellChecker(
            MockDatabaseManager(["wtf", "ok", "lmao"]), MockWiktionaryManager([])
        )
        self.assertEqual(mgr.get_misspelled(["ok", "wtf", "lmao"]), [])

    def test_check_against_wiktionary(self):
        mgr = SpellChecker(MockDatabaseManager([]), MockWiktionaryManager(["qqq"]))

        # this shouldn't be recognized as misspelled, since qqqqqqqqq is not a registered word yet
        self.assertEqual(mgr.get_misspelled(["qqqqqq"]), [])

        # register qqq
        self.assertEqual(mgr.get_misspelled(["qqq"]), [])

        # now it is misspelled
        self.assertEqual(mgr.get_misspelled(["qqqqqq"]), ["qqqqqq"])

    def test_check_correction_distance(self):
        mgr = SpellChecker(MockDatabaseManager([]), MockWiktionaryManager([]))

        # this word is not misspelled
        self.assertEqual(mgr.get_misspelled(["fixed"]), [])

        # this word is misspelled
        self.assertEqual(mgr.get_misspelled(["fixedqq"]), ["fixedqq"])

        # this word is misspelled so badly we assume it's a username or something
        self.assertEqual(mgr.get_misspelled(["fixedqqqqqqqqqq"]), [])


class MessageGeneratorTest(unittest.TestCase):

    def setUp(self):
        self.mgr = MessageGenerator()

    def tearDown(self):
        del self.mgr

    def test_respond_misspelled_words(self):
        # empty list
        self.assertEqual(self.mgr.respond_misspelled_words([]), "")

        # 1 word
        self.assertEqual(self.mgr.respond_misspelled_words(["bot"]), '"bot"')

        # many words
        self.assertEqual(
            self.mgr.respond_misspelled_words(["you", "are", "bot"]),
            '"you", "are", "bot"',
        )

    def test_respond_user_typos(self):
        self.assertEqual(
            self.mgr.respond_user_typos("zbkhan", 3), "zbkhan has made 3 typos."
        )


if __name__ == "__main__":
    unittest.main()
