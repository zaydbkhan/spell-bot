# https://stackoverflow.com/a/64296521

import unittest
from peewee import SqliteDatabase

from app.db import Member, Word
from app.data_layer import DatabaseManager, WiktionaryManager


class DatabaseManagerTest(unittest.TestCase):

    def __init__(self, methodName: str = "runTest") -> None:
        super().__init__(methodName)
        self.models = [Member, Word]
        self.test_db = SqliteDatabase(":memory:")

    def setUp(self):
        self.test_db.bind(self.models, bind_refs=False, bind_backrefs=False)
        self.test_db.connect()
        self.test_db.create_tables(self.models)
        self.mgr = DatabaseManager()

    def tearDown(self):
        self.test_db.drop_tables(self.models)
        self.test_db.close()
        del self.mgr

    def test_get_num_typos(self):
        # test create and then subsquent get
        self.assertEqual(self.mgr.get_num_typos(member_id=1), 0)
        self.assertEqual(self.mgr.get_num_typos(member_id=1), 0)

    def test_get_num_typos_negative(self):
        # test create and then subsquent get with negative number
        self.assertEqual(self.mgr.get_num_typos(member_id=-1), 0)
        self.assertEqual(self.mgr.get_num_typos(member_id=-1), 0)

    def test_update_num_typos(self):
        # test updating the number of typos for different user ids
        self.mgr.update_num_typos(1, 1)
        self.assertEqual(self.mgr.get_num_typos(1), 1)

        self.mgr.update_num_typos(1, 2)
        self.assertEqual(self.mgr.get_num_typos(1), 3)

        self.mgr.update_num_typos(2, 1)
        self.assertEqual(self.mgr.get_num_typos(2), 1)

    def test_update_num_typos_zero_negative(self):
        # test updating the number of typos for zeros and negatives
        self.mgr.update_num_typos(1, 1)
        self.assertEqual(self.mgr.get_num_typos(1), 1)

        self.mgr.update_num_typos(1, 0)
        self.assertEqual(self.mgr.get_num_typos(1), 1)

        self.mgr.update_num_typos(1, -2)
        self.assertEqual(self.mgr.get_num_typos(1), -1)

    def test_get_saved_slang_empty(self):
        # test an empty get
        self.assertEqual(self.mgr.get_saved_slang(), [])

    def test_create_new_slang(self):
        # test creating new slang words (and that getting properly returns a list)
        self.mgr.create_new_slang_word("leet")
        self.mgr.create_new_slang_word("1337")
        self.mgr.create_new_slang_word("lol")
        self.assertEqual(self.mgr.get_saved_slang(), ["leet", "1337", "lol"])

    def test_create_new_slang_long_word(self):
        # 256+ char word should be truncated to 255, create should not fail
        # not 100% sure if this is necessary since peewee might just fix it automatically?
        self.assertEqual(
            self.mgr.create_new_slang_word(
                "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
            ),
            True,
        )
        self.assertEqual(
            self.mgr.get_saved_slang(),
            [
                "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
            ],
        )


class WiktionaryManagerTest(unittest.TestCase):

    def setUp(self):
        self.mgr = WiktionaryManager()

    def tearDown(self):
        del self.mgr

    def test_get_word_exists(self):
        # "bot" is a word, so it should return True
        self.assertEqual(self.mgr.get_word_exists("bot"), True)

        # "lol" is slang, so it should return True
        self.assertEqual(self.mgr.get_word_exists("lol"), True)

        # certain common emojis will also return True
        self.assertEqual(self.mgr.get_word_exists("🔥"), True)

        # last I checked, this is not a word, but maybe it will be someday?
        self.assertEqual(self.mgr.get_word_exists("saodfjiowe"), False)


if __name__ == "__main__":
    unittest.main()
