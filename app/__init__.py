import nltk
import discord
from discord.ext.commands.bot import Bot

from app import db
from app.service_layer import EventHandler
from app.business_layer import SpellChecker, MessageGenerator, TypoTracker
from app.data_layer import DatabaseManager, WiktionaryManager

# download required packages
nltk.download("punkt")

# initialize database
db.init_db()

# configure bot intents
_intents = discord.Intents.default()
_intents.message_content = True

# set bot command prefix and initialize bot
_prefix = "spellbot "
bot = Bot(command_prefix=_prefix, intents=_intents)

# initialize event handler
_db_mgr = DatabaseManager(_prefix)
_wiki_mgr = WiktionaryManager()

event_handler = EventHandler(
    SpellChecker(_db_mgr, _wiki_mgr), MessageGenerator(), TypoTracker(_db_mgr)
)
