from abc import ABC, abstractmethod
import re
import string

import cleantext
import pkg_resources
import symspellpy
import symspellpy.suggest_item

from .data_layer import IDatabaseManager, IWiktionaryManager


# https://stackoverflow.com/questions/34293875/how-to-remove-punctuation-marks-from-a-string-in-python-3-x-using-translate
def tokenize_message(message: str) -> str:
    """Sanitize a message by removing all urls, emojis, punctuation, etc, then split it into individual words

    Args:
        message: The message to be sanitized.

    Returns:
        The sanitized message.
    """
    message = cleantext.clean(
        message,
        lower=False,
        no_urls=True,
        no_emails=True,
        no_phone_numbers=True,
        no_emoji=True,
        replace_with_url="",
        replace_with_email="",
        replace_with_phone_number="",
    )
    # replace all emoji and custom emoji
    message = re.sub(r":.+:", "", message)
    tokenized = message.split()
    stripped = [word.strip(string.punctuation) for word in tokenized]

    return stripped


class ISpellchecker(ABC):
    """Interface to identify misspelled words."""

    @abstractmethod
    def get_misspelled(self, word_list: list[str]) -> list[str]:
        """Get all the misspelled words from a list.

        Args:
            word_list: The list of words to be checked for misspellings.

        Returns:
            A list of misspelled words.
        """
        pass


class IMessageGenerator(ABC):
    """Interface for generating bot responses."""

    @abstractmethod
    def respond_misspelled_words(self, misspelled_words: list[str]) -> str:
        """Respond to misspellings in a member's message.

        Args:
            misspelled_words: The words that were misspelled in the member's message.

        Returns:
            A response to the member's message.
        """
        pass

    @abstractmethod
    def respond_user_typos(self, member_name: str, num_typos: int) -> str:
        """Respond to a request to see the number of typos a member has made.

        Args:
            member_name: The member's name (can be a mention).
            num_typos: The number of typos made by the member.

        Returns:
            A response to the command.
        """
        pass


class ITypoTracker(ABC):
    """Interface to get or update the number of typos made by a member."""

    @abstractmethod
    def add_typos(self, member_id: float, num_typos: int) -> bool:
        """Add to a member's total number of typos.

        Args:
            member_id: The member's account id.
            num_typos: The number of typos to add.

        Returns:
            True if the operation succeeded, False otherwise.
        """
        pass

    @abstractmethod
    def get_typos(self, member_id: float) -> int:
        """Get a member's total number of typos.

        Args:
            member_id: The member's account id.

        Returns:
            The member's total number of typos.
        """
        pass


class SpellChecker(ISpellchecker):
    """A real implementation of an ISpellchecker."""

    def __init__(
        self, database_manager: IDatabaseManager, wiktionary_manager: IWiktionaryManager
    ):
        """Creates a new SpellChecker.

        Args:
            database_manager: The IDatabaseManager to use.
            wiktionary_manager: The IWiktionaryManager to use.
        """
        self.db_mgr = database_manager
        self.wiki_mgr = wiktionary_manager
        self.spellchecker = symspellpy.SymSpell(max_dictionary_edit_distance=3)
        self.spellchecker.load_dictionary(
            pkg_resources.resource_filename(
                "symspellpy", "frequency_dictionary_en_82_765.txt"
            ),
            0,
            1,
        )
        for word in self.db_mgr.get_saved_slang():
            # load every word as 1 since we don't care about "correct" corrections
            self.spellchecker.create_dictionary_entry(word, 1)

    def get_misspelled(self, word_list: list[str]) -> list[str]:
        misspelled_words = []

        for word in word_list:
            if word == "" or word.isnumeric():
                continue

            lookup: list[symspellpy.suggest_item.SuggestItem] = (
                self.spellchecker.lookup(
                    word, symspellpy.Verbosity.TOP, transfer_casing=True
                )
            )

            if lookup != []:
                if lookup[0].distance == 0:
                    continue

            if self.wiki_mgr.get_word_exists(word):
                self.db_mgr.create_new_slang_word(word)
                # load every word as 1 since we don't care about "correct" corrections
                self.spellchecker.create_dictionary_entry(word, 1)
            elif lookup != []:
                misspelled_words.append(word)

        return misspelled_words


class MessageGenerator(IMessageGenerator):
    """A real implementation of an IMessageGenerator."""

    def respond_misspelled_words(self, misspelled_words: list[str]) -> str:
        str_to_return = ""

        if misspelled_words == []:
            return ""

        for word in misspelled_words:
            str_to_return += f'"{word}", '

        return str_to_return[: len(str_to_return) - 2]

    def respond_user_typos(self, member_name: str, num_typos: int) -> str:
        return f"{member_name} has made {num_typos} typos."


class TypoTracker(ITypoTracker):
    """A real implementation of an ITypoTracker."""

    def __init__(self, database_manager: IDatabaseManager):
        """Creates a new TypoTracker.

        Args:
            database_manager: The IDatabaseManager to use.
        """
        self.db_mgr = database_manager

    def add_typos(self, member_id: float, num_typos: int) -> bool:
        return self.db_mgr.update_num_typos(member_id, num_typos)

    def get_typos(self, member_id: float) -> int:
        return self.db_mgr.get_num_typos(member_id)
