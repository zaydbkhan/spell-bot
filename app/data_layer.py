from abc import ABC, abstractmethod
import requests

from .db import Member, Word


class IDatabaseManager(ABC):
    """Interface to manage database calls for typos and slang words."""

    @abstractmethod
    def get_num_typos(self, member_id: float) -> int:
        """Get the number of typos made by a member account.

        Args:
            member_id: The id of the account for which to retrieve the number of typos.

        Returns:
            The number of typos made by the member account.
        """
        pass

    @abstractmethod
    def update_num_typos(self, member_id: float, num_typos: int) -> bool:
        """Update the number of typos made by a member account.

        Args:
            member_id: The id of the account to be updated.
            num_typos: The number of typos to add to the member's count.

        Returns:
            True if the operation succeeded, False otherwise.
        """
        pass

    @abstractmethod
    def get_saved_slang(self) -> list[str]:
        """Get all saved slang words from the database.

        Returns:
            The list of slang words.
        """
        pass

    @abstractmethod
    def create_new_slang_word(self, word: str) -> bool:
        """Create a new slang word in the database.

        Args:
            word: The slang word to create.

        Returns:
            True if the operation succeeded, False otherwise.
        """
        pass


class IWiktionaryManager(ABC):
    """Interface to manage Wikitionary API calls."""

    @abstractmethod
    def get_word_exists(self, word: str) -> bool:
        """Query whether or not a word has a page on Wiktionary.

        Args:
            word: The word to search for.

        Returns:
            True if the word exists, False if not.
        """
        pass


class DatabaseManager(IDatabaseManager):
    """A real implementation of an IDatabaseManager."""

    def __init__(self, bot_command_prefix: str = ""):
        """Creates a new DatabaseManager.

        Args:
            bot_command_prefix: The bot's command prefix, to be registered to the database as a "real" word
        """
        if bot_command_prefix == "":
            return
        else:
            Word.get_or_create(text=bot_command_prefix.strip())

    def get_num_typos(self, member_id: float) -> int:
        try:
            return Member.get_by_id(member_id).typos
        except Member.DoesNotExist:
            Member.create(id=member_id, typos=0)
            return 0

    def update_num_typos(self, member_id: float, num_typos: int) -> bool:
        typos = self.get_num_typos(member_id)
        try:
            Member.set_by_id(member_id, {"typos": typos + num_typos})
            return True
        except:
            return False

    def get_saved_slang(self) -> list[str]:
        to_return = []
        words = Word.select()
        for word in words:
            to_return.append(word.text)
        return to_return

    def create_new_slang_word(self, word: str) -> bool:
        if len(word) > 255:
            word = word[:255]
        try:
            Word.create(text=word)
            return True
        except:
            return False


class WiktionaryManager(IWiktionaryManager):
    """A real implementation of an IWiktionaryManager."""

    def get_word_exists(self, word: str) -> bool:
        r = requests.get(
            f"https://en.wiktionary.org/api/rest_v1/page/definition/{word}"
        )
        if r.status_code == 200:
            return True
        else:
            return False


class MockDatabaseManager(IDatabaseManager):
    """Mock implementation of an IDatabaseManager for testing."""

    def __init__(self, saved_slang: list[str]):
        """Initialize with saved slang to be returned later."""
        self.saved_slang = saved_slang

    def get_num_typos(self, member_id: float) -> int:
        """Will return the member_id"""
        return member_id

    def update_num_typos(self, member_id: float, num_typos: int) -> bool:
        """Will return True"""
        return True

    def get_saved_slang(self) -> list[str]:
        """Will return saved slang"""
        return self.saved_slang

    def create_new_slang_word(self, word: str) -> bool:
        """Will return True"""
        return True


class MockWiktionaryManager(IWiktionaryManager):
    """Mock implementation of an IWiktionaryManager for testing."""

    def __init__(self, real_words: list[str]):
        """Initialize with all words that get_word_exists should return True for."""
        self.real_words = real_words

    def get_word_exists(self, word: str) -> bool:
        """Will return True if the word was passed in on initialization."""
        return word in self.real_words
