from discord import Member, Message

from . import business_layer
from .business_layer import ISpellchecker, IMessageGenerator, ITypoTracker


class EventHandler:

    def __init__(
        self,
        spell_checker: ISpellchecker,
        message_generator: IMessageGenerator,
        typo_tracker: ITypoTracker,
    ):
        """Creates a new EventHandler.

        Args:
            spell_checker: The ISpellchecker to use.
            message_generator: The IMessageGenerator to use.
            typo_tracker: The ITypoTracker to use.
        """
        self.spell_checker = spell_checker
        self.message_generator = message_generator
        self.typo_tracker = typo_tracker

    def check_typos(self, message: Message) -> str:
        """Identify the number and location of typos in a message,
        update the sender's total typos accordingly, and respond with a message.

        Args:
            message: The message to be checked for typos.

        Returns:
            A list of messages to send.
        """

        # santize message (remove punctuation, split contractions, etc.)
        # split by word
        # check spelling
        # iterate the number of user typos
        # mock the user for misspelled words

        tokenized = business_layer.tokenize_message(message.content)
        misspelled = self.spell_checker.get_misspelled(tokenized)

        if misspelled == []:
            return ""

        self.typo_tracker.add_typos(message.author.id, len(misspelled))
        return self.message_generator.respond_misspelled_words(misspelled)

    def get_user_typos(self, member: Member) -> str:
        """Respond to a request to get the number of typos for a certain member.

        Args:
            member: The member to get the number of typos for.

        Returns:
            A list of messages to send.
        """

        # get number of typos
        # insert number of typos into a funny message

        return self.message_generator.respond_user_typos(
            member.display_name, self.typo_tracker.get_typos(member.id)
        )
