import os
from peewee import *

_db = MySQLDatabase(
    host=os.getenv("DB_HOST"),
    user=os.getenv("DB_USER"),
    password=os.getenv("DB_PASSWORD"),
    database="spell_bot",
)


def open_db():
    _db.connect(reuse_if_open=True)


def close_db():
    _db.close()


def init_db():
    open_db()
    _db.create_tables([Member, Word])
    close_db()


class BaseModel(Model):
    class Meta:
        database = _db


class Member(BaseModel):
    """A database object representing a discord member.

    Attributes:
        member_id: The member's discord id.
        typos: The number of typos made by the member.
    """

    id = DoubleField(primary_key=True)
    typos = IntegerField()


class Word(BaseModel):
    """A database object representing an uncommon word (found on Wikitionary)

    Attributes:
        text: The word, spelled correctly.
    """

    text = CharField()
