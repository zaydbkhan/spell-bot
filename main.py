import os
import discord

from app import db
from app import bot, event_handler


@bot.event
async def on_ready():
    print(f"We have logged in as {bot.user}")


@bot.event
async def on_message(message: discord.Message):
    await bot.process_commands(message)

    if message.author == bot.user:
        return

    db.open_db()
    response = event_handler.check_typos(message)
    if response != "":
        await message.channel.send(response)
    db.close_db()


@bot.tree.command(
    name="typos",
    description="Returns the number of typos a user has made.",
)
@discord.app_commands.describe(user="The user to get typos for.")
async def get_typos(interaction: discord.Interaction, user: discord.Member):
    db.open_db()
    await interaction.response.send_message(event_handler.get_user_typos(user))
    db.close_db()


@bot.command()
async def sync(context):
    if str(context.author.id) == os.environ.get("OWNER_USER_ID"):
        try:
            await bot.tree.sync()
            await context.send("Synced command tree")
        except:
            await context.send("Failed to sync command tree")


bot.run(os.environ.get("BOT_TOKEN"))
